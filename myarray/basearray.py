# pylint: disable=line-too-long
# pylint: disable=invalid-name

import math
from typing import List
from typing import Tuple
from typing import Union
import array


class BaseArray:
    """
    Razred BaseArray, za učinkovito hranjenje in dostopanje do večdimenzionalnih podatkov. Vsi podatki so istega
    številskega tipa: int ali float.

    Pri ustvarjanju podamo obliko-dimenzije tabele. Podamo lahko tudi tip hranjenih podatkov (privzeto je int) in
    podatke za inicializacijo (privzeto vse 0).

    Primeri ustvarjanja:
    a = BaseArray((10,)) # tabela 10 vrednosti, tipa int, vse 0
    a = BaseArray((3, 3), dtype=float) # tabela 9 vrednost v 3x3 matriki, tipa float, vse 0.0
    a = BaseArray((2, 3), dtype=float, dtype=float, data=(1., 2., 1., 2., 1., 2.)) # tabela 6 vrednost v 2x3 matriki,
                                                                               #tipa float, vrednosti 1. in 2.
    a = BaseArray((2, 3, 4, 5), data=tuple(range(120))) # tabela dimenzij 2x3x4x5, tipa int, vrednosti od 0 do 120


    Do podatkov dostopamo z operatorjem []. Pri tem moramo paziti, da uporabimo indekse, ki so znotraj oblike tabele.
    Začnemo z indeksom 0, zadnji indeks pa je dolžina dimenzije -1.
     Primeri dostopanja :

    a = BaseArray((2, 3, 4, 5))
    a[0, 1, 2, 1] = 10 # nastavimo vrednost 10 na mesto 0, 1, 2, 1
    v = a[0, 0, 0, 0] # preberemo vrednost na mestu 0, 0, 0 ,0

    a[0, 3, 0, 0] # neveljaven indeks, ker je vrednost 3 enaka ali večja od dolžine dimenzije

    Velikost tabele lahko preberemo z lastnostjo 'shape', podatkovni tip pa z 'dtype'. Primer:

    a.shape # to vrne vrednost  (2, 3, 4, 5)
    a.dtype # vrne tip int

    Čez tabelo lahko iteriramo s for zanko, lahko uporabimo operatorje/ukaze 'reversed' in 'in'.

    a = BaseArray((4,2), data=(1, 2, 3, 4, 5, 6, 7, 8))
    for v in a: # for zanka izpiše vrednost od 1 do 8
        print(v)
    for v in reversed(a): # for zanka izpiše vrednost od 8 do 1
        print(v)

    2 in a # vrne True
    -1 in a # vrne False
    """

    def __init__(self, shape: Tuple[int], dtype: type = None, data: Union[Tuple, List] = None):
        """
        Create an initialize a multidimensional myarray of a selected data type.
        Init the myarray to 0.

        :param shape: tuple or list of integers, shape of constructed myarray
        :param dtype: type of data stored in myarray, float or int
        :param data: list of tuple of data values, must contain consistent types and
                     have a length that matches the shape.
        """

        _check_shape(shape)
        self.__shape = (*shape,)

        self.__steps = _shape_to_steps(shape)

        if dtype is None:
            dtype = float
        if dtype not in [int, float]:
            raise Exception('Type must by int or float.')
        self.__dtype = dtype

        n_elements = 1
        for s in self.__shape:
            n_elements *= s

        if data is None:
            if dtype == int:
                self.__data = array.array('i', [0] * n_elements)
            elif dtype == float:
                self.__data = array.array('f', [0] * n_elements)
        else:
            if len(data) != n_elements:
                raise Exception(f'shape {shape:} does not match provided data length {len(data):}')

            for d in data:
                if not isinstance(d, (int, float)):
                    raise Exception(f'provided data does not have a consistent type')
            self.__data = [d for d in data]

    @property
    def shape(self):
        """
        :return: a tuple representing the shape of the myarray.
        """
        return self.__shape

    @property
    def dtype(self):
        """
        :return: the type stored in the myarray.
        """
        return self.__dtype

    def __test_indice_in_range(self, ind):
        """
        Test if the given indice is in within the range of this myarray.
        :param ind: the indices used
        :return: True if indices are valid, False otherwise.
        """
        for ax in range(len(self.__shape)):
            if ind[ax] < 1 or ind[ax] > self.shape[ax]:
                raise Exception('indice {:}, axis {:d} out of bounds [1, {:}]'.format(ind, ax, self.__shape[ax]))

    def __getitem__(self, indice):
        """
        Return a value from this myarray.
        :param indice: indice to this myarray, integer or tuple
        :return: value at indice
        """
        if not isinstance(indice, tuple):
            indice = (indice,)

        if not _is_valid_indice(indice):
            raise Exception(f'{indice} is not a valid indice')

        if len(indice) != len(self.__shape):
            raise Exception(f'indice {indice} is not valid for this myarray')

        self.__test_indice_in_range(indice)
        key_lin = _multi_to_lin_ind(indice, self.__steps)
        return self.__data[key_lin]

    def __setitem__(self, indice, value):
        """
        Set a value in this myarray.
        :param indice: valued indice to a position, integer of tuple
        :param value: value to set
        :return: does not return
        """
        if not isinstance(indice, tuple):
            indice = (indice,)

        if not _is_valid_indice(indice):
            raise Exception(f'{indice} is not a valid indice')

        if len(indice) != len(self.__shape):
            raise Exception(f'indice {indice} is not valid for this myarray')

        self.__test_indice_in_range(indice)
        key_lin = _multi_to_lin_ind(indice, self.__steps)
        self.__data[key_lin] = value

    def __iter__(self):
        for v in self.__data:
            yield v

    def __reversed__(self):
        for v in reversed(self.__data):
            yield v

    def __contains__(self, item):
        return item in self.__data

    def print(self):
        l = max((len(str(x)) for x in self.__data), key=abs) + 2
        if len(self.shape) == 1:
            for i in self.__data:
                print(f"{i: >{l}}", end='')
        elif len(self.shape) == 2:
            i2 = 0
            for d in self.__data:
                if i2 == self.shape[1]:
                    i2 = 0
                    print()
                print(f"{d: >{l}}", end='')
                i2 += 1
            print()

        elif len(self.shape) == 3:
            i = 0
            i2 = 0
            i3 = 0
            print("level %i:" % i)
            for d in self.__data:
                print(f"{d: >{l}}", end='')
                i3 += 1

                if i3 == self.shape[2]:
                    i2 += 1
                    print()
                    i3 = 0

                if i2 == self.shape[1] and i < self.shape[0] - 1:
                    i += 1
                    print()
                    print("level %i:" % i)
                    i2 = 0
        return

    def sort(self, columns=False):
        if len(self.shape) == 1:
            self.__data = self.__mergeSort(self.__data)
        elif len(self.shape) == 2:
            toSort = []
            for i in range(1, (self.shape[1] if columns else self.shape[0]) + 1):
                for i2 in range(1, (self.shape[0] if columns else self.shape[1]) + 1):
                    toSort.append(self[i, i2] if not columns else self[i2, i])

                sorted = BaseArray.__mergeSort(toSort)

                for i2 in range(1, (self.shape[0] if columns else self.shape[1]) + 1):
                    if columns:
                        self[i2, i] = sorted[i2 - 1]
                    else:
                        self[i, i2] = sorted[i2 - 1]
                toSort.clear()

    @staticmethod
    def __mergeSort(arr):
        if len(arr) <= 1:
            return arr

        left = []
        right = []

        for i in range(len(arr)):
            if i < len(arr) / 2:
                left.append(arr[i])
            else:
                right.append(arr[i])

        left = BaseArray.__mergeSort(left)
        right = BaseArray.__mergeSort(right)

        return BaseArray.__merge(left, right)

    @staticmethod
    def __merge(left: List, right: List):
        result = []
        while len(left) > 0 and len(right) > 0:
            if left[0] <= right[0]:
                result.append(left[0])
                del left[0]
            else:
                result.append(right[0])
                del right[0]

        for i in range(len(left)):
            result.append(left[i])

        for i in range(len(right)):
            result.append(right[i])

        return result

    def find(self, value):
        res = []
        if len(self.shape) == 1:
            i = 0
            for d in self.__data:
                i += 1
                if d == value:
                    res.append(i)
        elif len(self.shape) == 2:
            i = 1
            i2 = 0
            for d in self.__data:
                i2 += 1
                if d == value:
                    res.append((i, i2))
                if i2 == self.shape[1]:
                    i += 1
                    i2 = 0

        elif len(self.shape) == 3:
            i = 1
            i2 = 1
            i3 = 0

            for d in self.__data:
                i3 += 1
                if d == value:
                    res.append((i, i2, i3))
                if i3 == self.shape[2]:
                        i2 += 1
                        i3 = 0
                if i2 == self.shape[1] + 1:
                    i += 1
                    i2 = 1

        return res

    def __add__(self, other):
        a = 0
        if type(other) is BaseArray:
            if other.shape == self.shape:
                a = BaseArray(self.shape, dtype=float if self.dtype == float or other.dtype == float else int)
                a.__data = [x + y for x, y in zip(self.__data, other.__data)]
            else:
                raise ValueError(f"Arrays are not the same size: {self.shape} expected. Got {other.shape}.")
        elif type(other) in [int, float]:
            a = BaseArray(self.shape, dtype=float if self.dtype == float or type(other) == float else int)
            a.__data = [x + other for x in self.__data]
        return a

    def __radd__(self, other):
        return self + other

    def __sub__(self, other):
        if type(other) is BaseArray:
            if other.shape == self.shape:
                a = BaseArray(self.shape, dtype=float if self.dtype == float or other.dtype == float else int)
                a.__data = [x - y for x, y in zip(self.__data, other.__data)]
                return a
            else:
                raise ValueError(f"Arrays are not the same size: {self.shape} expected. Got {other.shape}.")
        elif type(other) in [int, float]:
            a = BaseArray(self.shape, dtype=float if self.dtype == float or type(other) == float else int)
            a.__data = [x - other for x in self.__data]
            return a

    def __mul__(self, other):
        if type(other) in [int, float]:
            a = BaseArray(self.shape, dtype=float if self.dtype == float or type(other) == float else int)
            a.__data = [x * other for x in self.__data]
            return a
        elif type(other) is BaseArray:
            if other.shape == self.shape:
                if len(self.shape) == 2:
                    if self.shape[1] != other.shape[0]:
                        raise ValueError(
                            f"Matrices are not the correct size for multiplication: {self.shape} and {other.shape}.")
                    a = BaseArray((self.shape[0], other.shape[1]),
                                  dtype=float if self.dtype != other.dtype else self.dtype)
                    for i in range(1, self.shape[0] + 1):
                        for i2 in range(1, other.shape[1] + 1):
                            for i3 in range(1, other.shape[0] + 1):
                                a[i, i2] += self[i, i3] * other[i3, i2]
                    return a
                else:
                    a = BaseArray(self.shape, dtype=float if self.dtype == float or other.dtype == float else int)
                    a.__data = [x * y for x, y in zip(self.__data, other.__data)]
                return a
            else:
                raise ValueError(f"Arrays are not the same size: {self.shape} expected. Got {other.shape}.")

    def __rmul__(self, other):
        return self * other

    def __truediv__(self, other):
        if type(other) in [int, float]:
            a = BaseArray(self.shape, dtype=float)
            a.__data = [x / other for x in self.__data]
            return a
        elif type(other) is BaseArray:
            if other.shape == self.shape:
                if 0 in other:
                    raise ArithmeticError("Division by 0")
                a = BaseArray(self.shape, dtype=float)
                a.__data = [x / y for x, y in zip(self.__data, other.__data)]
                return a
            else:
                raise ValueError(f"Arrays are not the same size: {self.shape} expected. Got {other.shape}.")

    def log(self, base=math.e):
        a = BaseArray(self.shape, dtype=float)
        a.__data = [math.log(x) if base == math.e else math.log(x, base) for x in self.__data]
        return a

    def pow(self, power):
        a = BaseArray(self.shape, dtype=self.dtype)
        a.__data = [math.pow(x, power) for x in self.__data]
        return a


def _check_shape(shape):
    if not isinstance(shape, (tuple, list)):
        raise Exception(f'shape {shape:} is not a tuple or list')
    for dim in shape:
        if not isinstance(dim, int):
            raise Exception(f'shape {shape:} contains a non integer {dim:}')


def _multi_ind_iterator(multi_inds, shape):
    inds = multi_inds[0]
    s = shape[0]

    if isinstance(inds, slice):
        start, stop, step = inds.start, inds.stop, inds.step
        if start is None:
            start = 1
        if step is None:
            step = 1
        if stop is not None:
            stop += 1
        inds_itt = range(0, s + 1)[start:stop:step]
    else:  # type(inds) is int:
        inds_itt = (inds,)

    for ind in inds_itt:
        if len(shape) > 1:
            for ind_tail in _multi_ind_iterator(multi_inds[1:], shape[1:]):
                yield (ind,) + ind_tail
        else:
            yield (ind,)


def _shape_to_steps(shape):
    dim_steps_rev = []
    s = 1
    for d in reversed(shape):
        dim_steps_rev.append(s)
        s *= d
    steps = tuple(reversed(dim_steps_rev))
    return steps


def _multi_to_lin_ind(inds, steps):
    i = 0
    for n, s in enumerate(steps):
        i += (inds[n] - 1) * s
    return i


def _is_valid_indice(indice):
    if isinstance(indice, tuple):
        if not indice:
            return False
        for i in indice:
            if not isinstance(i, int):
                return False
    elif not isinstance(indice, int):
        return False
    return True
