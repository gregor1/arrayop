import io
import sys
from io import StringIO
from unittest import TestCase
from myarray.basearray import BaseArray
import myarray.basearray as ndarray


class TestArray(TestCase):
    def test___init__(self):

        # simple init tests, other things are tester later
        try:
            a = BaseArray((2,))
            a = BaseArray([2, 5])
            a = BaseArray((2, 5), dtype=int)
            a = BaseArray((2, 5), dtype=float)
        except Exception as e:
            self.fail(f'basic constructor test failed, with exception {e:}')

        # test invalid parameters as shape
        with self.assertRaises(Exception) as cm:
            a = BaseArray(5)
        self.assertEqual(('shape 5 is not a tuple or list',),
                         cm.exception.args)

        # test invalid value in shape
        with self.assertRaises(Exception) as cm:
            a = BaseArray((4, 'a'))
        self.assertEqual(('shape (4, \'a\') contains a non integer a',),
                         cm.exception.args)

        # TODO test invalid dtype

        # test valid data param, list, tuple
        a = BaseArray((3, 4), dtype=int, data=list(range(12)))
        a = BaseArray((3, 4), dtype=int, data=tuple(range(12)))

        # test invalid data param
        # invalid data length
        with self.assertRaises(Exception) as cm:
            a = BaseArray((2, 4), dtype=int, data=list(range(12)))
        self.assertEqual(('shape (2, 4) does not match provided data length 12',),
                         cm.exception.args)

        # inconsistent data type
        with self.assertRaises(Exception) as cm:
            a = BaseArray((2, 2), dtype=int, data=(4, 'a', (), 2.0))
        self.assertEqual(('provided data does not have a consistent type',),
                         cm.exception.args)

    def test_shape(self):
        a = BaseArray((5,))
        self.assertTupleEqual(a.shape, (5,))
        a = BaseArray((2, 5))
        self.assertTupleEqual(a.shape, (2, 5))
        a = BaseArray((3, 2, 5))
        self.assertEqual(a.shape, (3, 2, 5))

    def test_dtype(self):
        # test if setting a type returns the set type
        a = BaseArray((1,))
        self.assertIsInstance(a[1], float)
        a = BaseArray((1,), int)
        self.assertIsInstance(a[1], int)
        a = BaseArray((1,), float)
        self.assertIsInstance(a[1], float)

    def test_get_set_item(self):
        # test if setting a value returns the value
        a = BaseArray((1,))
        a[1] = 1
        self.assertEqual(1, a[1])
        # test for multiple dimensions
        a = BaseArray((2, 1))
        a[1, 1] = 1
        a[2, 1] = 1
        self.assertEqual(1, a[1, 1])
        self.assertEqual(1, a[2, 1])

        # test with invalid indices
        a = BaseArray((2, 2))
        with self.assertRaises(Exception) as cm:
            a[4, 1] = 0
        self.assertEqual(('indice (4, 1), axis 0 out of bounds [1, 2]',),
                         cm.exception.args)

        # test invalid type of ind
        with self.assertRaises(Exception) as cm:
            a[1, 1.1] = 0
        self.assertEqual(cm.exception.args,
                         ('(1, 1.1) is not a valid indice',))

        with self.assertRaises(Exception) as cm:
            a[1, 'a'] = 0
        self.assertEqual(('(1, \'a\') is not a valid indice',),
                         cm.exception.args)

        # test invalid number of ind
        with self.assertRaises(Exception) as cm:
            a[2] = 0
        self.assertEqual(('indice (2,) is not valid for this myarray',),
                         cm.exception.args)

        # test data intialized via data parameter
        a = BaseArray((2, 3, 4), dtype=int, data=tuple(range(24)))
        self.assertEqual(3, a[1, 1, 4])
        self.assertEqual(13, a[2, 1, 2])

        # TODO enter invalid type

    def test_iter(self):
        a = BaseArray((2, 3), dtype=int)
        a[1, 1] = 1
        a[1, 2] = 2
        a[1, 3] = 3
        a[2, 1] = 4
        a[2, 2] = 5
        a[2, 3] = 6

        a_vals = [v for v in a]
        self.assertListEqual([1, 2, 3, 4, 5, 6], a_vals)

    def test_reversed(self):
        a = BaseArray((2, 3), dtype=int)
        a[1, 1] = 1
        a[1, 2] = 2
        a[1, 3] = 3
        a[2, 1] = 4
        a[2, 2] = 5
        a[2, 3] = 6

        a_vals = [v for v in reversed(a)]
        self.assertListEqual([6, 5, 4, 3, 2, 1], a_vals)

    def test_contains(self):
        a = BaseArray((1,), dtype=float)
        a[1] = 1

        self.assertIn(1., a)
        self.assertIn(1, a)
        self.assertNotIn(0, a)

    def test_shape_to_steps(self):
        shape = (4, 2, 3)
        steps_exp = (6, 3, 1)
        steps_res = ndarray._shape_to_steps(shape)
        self.assertTupleEqual(steps_exp, steps_res)

        shape = [1]
        steps_exp = (1,)
        steps_res = ndarray._shape_to_steps(shape)
        self.assertTupleEqual(steps_exp, steps_res)

        shape = []
        steps_exp = ()
        steps_res = ndarray._shape_to_steps(shape)
        self.assertTupleEqual(steps_exp, steps_res)

    def test_multi_to_lin_ind(self):
        # shape = 4, 2,
        # 0 1
        # 2 3
        # 4 5
        # 6 7
        steps = 2, 1
        multi_lin_inds_pairs = (((1, 1), 0),
                                ((2, 2), 3),
                                ((4, 1), 6),
                                ((4, 2), 7))
        for multi_inds, lin_ind_exp in multi_lin_inds_pairs:
            lin_ind_res = ndarray._multi_to_lin_ind(multi_inds, steps)
            self.assertEqual(lin_ind_exp, lin_ind_res)

        # shape = 2, 3, 4
        #
        # 0 1 2 3
        # 4 5 6 7
        #
        #  8  9 10 11
        # 12 13 14 15
        steps = 8, 4, 1
        multi_lin_inds_pairs = (((1, 1, 1), 0),
                                ((2, 2, 1), 12),
                                ((1, 1, 4), 3),
                                ((2, 2, 3), 14))

        for multi_inds, lin_ind_exp in multi_lin_inds_pairs:
            lin_ind_res = ndarray._multi_to_lin_ind(multi_inds, steps)
            self.assertEqual(lin_ind_res, lin_ind_exp)

    def test_is_valid_instance(self):
        self.assertTrue(ndarray._is_valid_indice(2))
        self.assertTrue(ndarray._is_valid_indice((2, 1)))

        self.assertFalse(ndarray._is_valid_indice(()))
        self.assertFalse(ndarray._is_valid_indice([2]))
        self.assertFalse(ndarray._is_valid_indice(2.0))
        self.assertFalse(ndarray._is_valid_indice((2, 3.)))

    def test_multi_ind_iterator(self):
        shape = (30,)
        ind = (slice(10, 20, 3),)
        ind_list_exp = ((10,),
                        (13,),
                        (16,),
                        (19,))
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

        shape = (5, 5, 5)
        ind = (2, 2, 2)
        ind_list_exp = ((2, 2, 2),)
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

        ind = (2, slice(3), 2)
        ind_list_exp = ((2, 1, 2),
                        (2, 2, 2),
                        (2, 3, 2),
                        )
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

        ind = (slice(None, None, 2), slice(3), slice(2, 4))
        ind_list_exp = ((1, 1, 2), (1, 1, 3), (1, 1, 4),
                        (1, 2, 2), (1, 2, 3), (1, 2, 4),
                        (1, 3, 2), (1, 3, 3), (1, 3, 4),
                        (3, 1, 2), (3, 1, 3), (3, 1, 4),
                        (3, 2, 2), (3, 2, 3), (3, 2, 4),
                        (3, 3, 2), (3, 3, 3), (3, 3, 4),
                        (5, 1, 2), (5, 1, 3), (5, 1, 4),
                        (5, 2, 2), (5, 2, 3), (5, 2, 4),
                        (5, 3, 2), (5, 3, 3), (5, 3, 4),
                        )
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

    def test_print_2d(self):
        a = BaseArray((2, 3), dtype=int, data=(-30000, -2.3453235, -1, 0, 1.435, 2))

        expected = "      -30000  -2.3453235          -1\n           0       1.435           2\n"
        old_stdout = sys.stdout
        sys.stdout = mystdout = StringIO()
        a.print()
        sys.stdout = old_stdout

        self.assertMultiLineEqual(mystdout.getvalue(), expected)

    def test_print_3d(self):
        a = BaseArray((2, 3, 2), data=(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12))

        expected = "level 0:\n   1   2\n   3   4\n   5   6\n\nlevel 1:\n   7   8\n   9  10\n  11  12\n"
        old_stdout = sys.stdout
        sys.stdout = mystdout = StringIO()
        a.print()
        sys.stdout = old_stdout

        self.assertMultiLineEqual(mystdout.getvalue(), expected)

    def test_1d_sort(self):
        a = BaseArray((6,), dtype=int, data=(5, 4, 3, 2, 1, 10))
        expected = BaseArray((6,), dtype=int, data=(1, 2, 3, 4, 5, 10))

        a.sort()

        self.assertListEqual([x for x in a], [x for x in expected])

    def test_2d_sort_lines(self):
        a = BaseArray((2, 3), dtype=int, data=(10, -1, -10, 2, 1, 0))
        expected = BaseArray((2, 3), dtype=int, data=(-10, -1, 10, 0, 1, 2))

        a.sort()

        self.assertListEqual([x for x in a], [x for x in expected])

    def test_2d_sort_columns(self):
        a = BaseArray((2, 3), dtype=int, data=(10, -1, -10, 2, 1, 0))
        expected = BaseArray((2, 3), dtype=int, data=(2, -1, -10, 10, 1, 0))

        a.sort(True)

        self.assertListEqual([x for x in a], [x for x in expected])

    def test_find_1d(self):
        a = BaseArray((6,), dtype=int, data=(0, 2, 0, 2, 0, 2))
        expected = [2, 4, 6]

        result = a.find(2)

        self.assertListEqual(result, expected)

    def test_find_2d(self):
        a = BaseArray((3, 3), dtype=int, data=(0, 0, 0, 2, 0, 0, 0, 2, 2))
        expected = [(2, 1), (3, 2), (3, 3)]

        result = a.find(2)

        self.assertListEqual(result, expected)

    def test_find_3d(self):
        a = BaseArray((2, 3, 2), dtype=int, data=(0, 0, 0, 2, 0, 0, 0, 2, 2, 0, 0, 2))
        expected = [(1, 2, 2), (2, 1, 2), (2, 2, 1), (2, 3, 2)]

        result = a.find(2)

        self.assertListEqual(result, expected)

    def test_add_1d(self):
        a = BaseArray((6,), dtype=int, data=(0, 1, 3, 4, 5, 6))
        c = BaseArray((6,), dtype=float, data=(1.1, 2, 3.4, 4, 5, 6))
        expected = BaseArray((6,), dtype=int, data=(1.1, 3, 6.4, 8, 10, 12))
        expected2 = BaseArray((6,), dtype=int, data=(3, 4, 6, 7, 8, 9))

        result = a + c
        result2 = a + 3

        self.assertListEqual([x for x in result], [x for x in expected])
        self.assertListEqual([x for x in result2], [x for x in expected2])
        self.assertTrue(result.dtype is float)
        self.assertTrue(result2.dtype is int)

    def test_add_2d(self):
        a = BaseArray((3, 3), dtype=int, data=(0, 1, 3, 4, 5, 6, 1, 32, 5))
        c = BaseArray((3, 3), dtype=float, data=(1.1, 2, 3.4, 4, 5, 6, 7, 8.4, 9))
        expected = BaseArray((3, 3), dtype=float, data=(1.1, 3, 6.4, 8, 10, 12, 8, 40.4, 14))
        expected2 = BaseArray((3, 3), dtype=int, data=(3, 4, 6, 7, 8, 9, 4, 35, 8))

        result = a + c
        result2 = a + 3

        self.assertListEqual([x for x in result], [x for x in expected])
        self.assertListEqual([x for x in result2], [x for x in expected2])
        self.assertTrue(result.dtype is float)
        self.assertTrue(result2.dtype is int)

    def test_add_3d(self):
        a = BaseArray((2, 3, 2), dtype=int, data=(0, 1, 3, 4, 5, 6, 1, 32, 5, 10, 43, 45))
        c = BaseArray((2, 3, 2), dtype=float, data=(1.1, 2, 3.4, 4, 5, 6, 7, 8.4, 9, 10.6, 34.6, 5.1))
        expected = BaseArray((2, 3, 2), dtype=float, data=(1.1, 3, 6.4, 8, 10, 12, 8, 40.4, 14, 20.6, 77.6, 50.1))
        expected2 = BaseArray((2, 3, 2), dtype=int, data=(5, 6, 8, 9, 10, 11, 6, 37, 10, 15, 48, 50))

        result = a + c
        result2 = a + 5

        self.assertListEqual([x for x in result], [x for x in expected])
        self.assertListEqual([x for x in result2], [x for x in expected2])
        self.assertTrue(result.dtype is float)
        self.assertTrue(result2.dtype is int)

    def test_add_error(self):
        a = BaseArray((3, 3), dtype=int, data=(0, 1, 3, 4, 5, 6, 1, 32, 5))
        c = BaseArray((3, 2), dtype=float, data=(1.1, 2, 3.4, 4, 5, 6))

        with self.assertRaises(ValueError):
            a + c

    def test_sub_1d(self):
        a = BaseArray((6,), dtype=int, data=(0, 1, 3, 4, 5, 6))
        c = BaseArray((6,), dtype=float, data=(1.1, 2, 4.5, 4, 8.5, 6))
        expected = BaseArray((6,), dtype=float, data=(-1.1, -1, -1.5, 0, -3.5, 0))
        expected2 = BaseArray((6,), dtype=int, data=(-3, -2, 0, 1, 2, 3))

        result = a - c
        result2 = a - 3

        self.assertListEqual([x for x in result], [x for x in expected])
        self.assertListEqual([x for x in result2], [x for x in expected2])
        self.assertTrue(result.dtype is float)
        self.assertTrue(result2.dtype is int)

    def test_sub_2d(self):
        a = BaseArray((3, 3), dtype=int, data=(0, 1, 3, 4, 5, 6, 1, 32, 5))
        c = BaseArray((3, 3), dtype=float, data=(1.1, 2, 3.5, 4, 5, 6, 7, 8.4, 9))
        expected = BaseArray((3, 3), dtype=float, data=(-1.1, -1, -0.5, 0, 0, 0, -6, 23.6, -4))
        expected2 = BaseArray((3, 3), dtype=int, data=(-3, -2, 0, 1, 2, 3, -2, 29, 2))

        result = a - c
        result2 = a - 3

        self.assertListEqual([x for x in result], [x for x in expected])
        self.assertListEqual([x for x in result2], [x for x in expected2])
        self.assertTrue(result.dtype is float)
        self.assertTrue(result2.dtype is int)

    def test_sub_3d(self):
        a = BaseArray((2, 3, 2), dtype=int, data=(0, 1, 3, 4, 5, 6, 1, 32, 5, 10, 43, 45))
        c = BaseArray((2, 3, 2), dtype=float, data=(1.1, 2, 3.5, 4, 5, 6, 7, 8.25, 9, 10.5, 34.75, 5.1))
        expected = BaseArray((2, 3, 2), dtype=float, data=(-1.1, -1, -0.5, 0, 0, 0, -6, 23.75, -4, -0.5, 8.25, 39.9))
        expected2 = BaseArray((2, 3, 2), dtype=int, data=(-5, -4, -2, -1, 0, 1, -4, 27, 0, 5, 38, 40))

        result = a - c
        result2 = a - 5

        self.assertListEqual([x for x in result], [x for x in expected])
        self.assertListEqual([x for x in result2], [x for x in expected2])
        self.assertTrue(result.dtype is float)
        self.assertTrue(result2.dtype is int)

    def test_sub_error(self):
        a = BaseArray((3, 3), dtype=int, data=(0, 1, 3, 4, 5, 6, 1, 32, 5))
        c = BaseArray((3, 2), dtype=float, data=(1.1, 2, 3.4, 4, 5, 6))

        with self.assertRaises(ValueError):
            a - c

    def test_mul_1d(self):
        a = BaseArray((6,), dtype=int, data=(0, 1, 3, 4, 5, 6))
        c = BaseArray((6,), dtype=float, data=(1.1, 2, 4.5, 4, 8.5, 6))
        expected = BaseArray((6,), dtype=float, data=(0.0, 2, 13.5, 16, 42.5, 36))
        expected2 = BaseArray((6,), dtype=int, data=(0, 3, 9, 12, 15, 18))

        result = a * c
        result2 = a * 3

        self.assertListEqual([x for x in result], [x for x in expected])
        self.assertListEqual([x for x in result2], [x for x in expected2])
        self.assertTrue(result.dtype is float)
        self.assertTrue(result2.dtype is int)

    def test_mul_2d(self):
        a = BaseArray((3, 3), dtype=int, data=(0, 1, 3, 4, 5, 6, 1, 32, 5))
        c = BaseArray((3, 3), dtype=float, data=(1.1, 2, 3.5, 4, 5, 6, 7, 8.25, 9))
        expected = BaseArray((3, 3), dtype=float, data=(25.0, 29.75, 33.0, 66.4, 82.5, 98.0, 164.1, 203.25, 240.5))
        expected2 = BaseArray((3, 3), dtype=int, data=(0, 3, 9, 12, 15, 18, 3, 96, 15))

        result = a * c
        result2 = a * 3

        for x, y in zip(result, expected):
            self.assertAlmostEqual(x, y, places=4)

        for x, y in zip(result2, expected2):
            self.assertAlmostEqual(x, y, places=4)

        self.assertTrue(result.dtype is float)
        self.assertTrue(result2.dtype is int)

    def test_mul_3d(self):
        a = BaseArray((2, 3, 2), dtype=int, data=(0, 1, 3, 4, 5, 6, 1, 32, 5, 10, 43, 45))
        c = BaseArray((2, 3, 2), dtype=float, data=(1.1, 2, 3.5, 4, 5, 6, 7, 8.25, 9, 10.5, 34.75, 5))
        expected = BaseArray((2, 3, 2), dtype=float, data=(0.0, 2, 10.5, 16, 25, 36, 7, 264.0, 45, 105.0, 1494.25, 225))
        expected2 = BaseArray((2, 3, 2), dtype=int, data=(0, 5, 15, 20, 25, 30, 5, 160, 25, 50, 215, 225))

        result = a * c
        result2 = a * 5

        self.assertListEqual([x for x in result], [x for x in expected])
        self.assertListEqual([x for x in result2], [x for x in expected2])
        self.assertTrue(result.dtype is float)
        self.assertTrue(result2.dtype is int)

    def test_mul_error(self):
        a = BaseArray((3, 3), dtype=int, data=(0, 1, 3, 4, 5, 6, 1, 32, 5))
        c = BaseArray((1, 6), dtype=float, data=(1.1, 2, 3.4, 4, 5, 6))

        with self.assertRaises(ValueError):
            a * c

    def test_div_1d(self):
        a = BaseArray((6,), dtype=int, data=(0, 1, 10, 4, 5, 10))
        c = BaseArray((6,), dtype=float, data=(1.1, 2, 4.5, 4, 8.5, 5))
        expected = BaseArray((6,), dtype=float, data=(0.0, 0.5, 2.2222222222222223, 1.0, 0.5882352941176471, 2.0))
        expected2 = BaseArray((6,), dtype=int, data=(0.0, 0.25, 2.5, 1.0, 1.25, 2.5))

        result = a / c
        result2 = a / 4

        self.assertListEqual([x for x in result], [x for x in expected])
        self.assertListEqual([x for x in result2], [x for x in expected2])
        self.assertTrue(result.dtype is float)
        self.assertTrue(result2.dtype is float)

    def test_div_2d(self):
        a = BaseArray((3, 3), dtype=int, data=(0, 1, 3, 4, 5, 6, 1, 32, 5))
        c = BaseArray((3, 3), dtype=float, data=(1.1, 2, 3.5, 4, 5, 6, 7, 8.25, 9))
        expected = BaseArray((3, 3), dtype=float, data=(
            0.0, 0.5, 0.8571428571428571, 1.0, 1.0, 1.0, 0.14285714285714285, 3.878787878787879, 0.5555555555555556))
        expected2 = BaseArray((3, 3), dtype=float, data=(0.0, 0.25, 0.75, 1.0, 1.25, 1.5, 0.25, 8.0, 1.25))

        result = a / c
        result2 = a / 4

        self.assertListEqual([x for x in result], [x for x in expected])
        self.assertListEqual([x for x in result2], [x for x in expected2])
        self.assertTrue(result.dtype is float)
        self.assertTrue(result2.dtype is float)

    def test_div_3d(self):
        a = BaseArray((2, 3, 2), dtype=int, data=(0, 1, 3, 4, 5, 6, 1, 32, 5, 10, 43, 45))
        c = BaseArray((2, 3, 2), dtype=float, data=(1.1, 2, 3.5, 4, 5, 6, 7, 8.25, 9, 10.5, 34.75, 5))
        expected = BaseArray((2, 3, 2), dtype=float, data=(
            0.0, 0.5, 0.8571428571428571, 1.0, 1.0, 1.0, 0.14285714285714285, 3.878787878787879, 0.5555555555555556,
            0.9523809523809523, 1.2374100719424461, 9.0))
        expected2 = BaseArray((2, 3, 2), dtype=int,
                              data=(0.0, 0.25, 0.75, 1.0, 1.25, 1.5, 0.25, 8.0, 1.25, 2.5, 10.75, 11.25))

        result = a / c
        result2 = a / 4

        self.assertListEqual([x for x in result], [x for x in expected])
        self.assertListEqual([x for x in result2], [x for x in expected2])
        self.assertTrue(result.dtype is float)
        self.assertTrue(result2.dtype is float)

    def test_div_error(self):
        a = BaseArray((3, 3), dtype=int, data=(0, 1, 3, 4, 5, 6, 1, 32, 5))
        c = BaseArray((1, 6), dtype=float, data=(1.1, 2, 3.4, 4, 5, 6))

        with self.assertRaises(ValueError):
            a / c

    def test_log_1d(self):
        a = BaseArray((6,), dtype=int, data=(10, 1, 10, 4, 5, 10))
        expected = BaseArray((6,), dtype=float, data=(
            2.302585092994046, 0.0, 2.302585092994046, 1.3862943611198906, 1.6094379124341003, 2.302585092994046))

        result = a.log()

        self.assertListEqual([x for x in result], [x for x in expected])
        self.assertTrue(result.dtype is float)

    def test_log_2d(self):
        a = BaseArray((3, 3), dtype=int, data=(10, 1, 3, 4, 5, 6, 1, 32, 5))
        expected = BaseArray((3, 3), dtype=float, data=(
            2.302585092994046, 0.0, 1.0986122886681096, 1.3862943611198906, 1.6094379124341003, 1.791759469228055, 0.0,
            3.4657359027997265, 1.6094379124341003))

        result = a.log()

        for x, y in zip(result, expected):
            self.assertAlmostEqual(x, y)
        self.assertTrue(result.dtype is float)

    def test_log_3d(self):
        a = BaseArray((2, 3, 2), dtype=int, data=(10, 1, 3, 4, 5, 6, 1, 32, 5, 10, 43, 45))
        expected = BaseArray((2, 3, 2), dtype=int,
                             data=(2.302585092994046, 0.0, 1.0986122886681096, 1.3862943611198906, 1.6094379124341003,
                                   1.791759469228055, 0.0, 3.4657359027997265, 1.6094379124341003, 2.302585092994046,
                                   3.7612001156935624, 3.8066624897703196))

        result = a.log()

        for x, y in zip(result, expected):
            self.assertAlmostEqual(x, y)
        self.assertTrue(result.dtype is float)

    def test_pow_1d(self):
        a = BaseArray((6,), dtype=int, data=(10, 1, 10, 4, 5, 10))
        expected = BaseArray((6,), dtype=float, data=(100.0, 1.0, 100.0, 16.0, 25.0, 100.0))

        result = a.pow(2)

        self.assertListEqual([x for x in result], [x for x in expected])
        self.assertTrue(result.dtype is a.dtype)

    def test_pow_2d(self):
        a = BaseArray((3, 3), dtype=int, data=(10, 1, 3, 4, 5, 6, 1, 32, 5))
        expected = BaseArray((3, 3), dtype=float, data=(100.0, 1.0, 9.0, 16.0, 25.0, 36.0, 1.0, 1024.0, 25.0))

        result = a.pow(2)

        self.assertListEqual([x for x in result], [x for x in expected])
        self.assertTrue(result.dtype is a.dtype)

    def test_pow_3d(self):
        a = BaseArray((2, 3, 2), dtype=int, data=(10, 1, 3, 4, 5, 6, 1, 32, 5, 10, 43, 45))
        expected = BaseArray((2, 3, 2), dtype=int,
                             data=(100.0, 1.0, 9.0, 16.0, 25.0, 36.0, 1.0, 1024.0, 25.0, 100.0, 1849.0, 2025.0))

        result = a.pow(2)

        self.assertListEqual([x for x in result], [x for x in expected])
        self.assertTrue(result.dtype is result.dtype)
